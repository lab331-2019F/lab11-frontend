import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FileNotFoundComponent } from './shared/file-not-found/file-not-found.component';
import { CourseListComponent } from './course/list/list.component';
import { CourseAddComponent } from './course/add/add.component';


const appRoutes: Routes = [
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: 'course/list',
    component: CourseListComponent
  },
  {
    path: 'course/add',
    component: CourseAddComponent
  },
  { path: '**', component: FileNotFoundComponent }

];

@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {
}
